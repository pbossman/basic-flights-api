This is a very basic python flask API I threw together to demonstrate python connectivity with MariaDB Xpand.  I didn't really think anyone would want to use it, but people like to try it all out, so here's the code!

1. Follow presentation steps to import the travel database into Xpand.
2. In config/config.json - change username and password authorized to access the travel database.
3. cd code
4. python3 flightAPI.py
5. http://localhost:8081/flights/<origin>/<dest>/<fl_date>

http://localhost:8081/flights/SRQ/ATL/2020-01-05

Note: The data used in the demo contained flights for January, 2020.
