import mariadb
import json
from flask import Flask

fConfig = open("../config/config.json",)
config = json.load(fConfig)['mariadbTest']

app = Flask(__name__)

try:                                                        # try to connect to database
    cnx = mariadb.connect(
        host=config['dbhost'],
        port=config['dbport'],
        user=config['dbuser'],
        password=config['dbpassword'],
        database=config['database'])
    cnx.auto_reconnect = True
except mariadb.Error as err:                        # if we fail, write message and exit.
    print("Database initial connect failed: {}".format(err))
    exit()

def runSelectSQL(sql, sqlParms, keyLabel):
    try:
        cursor = cnx.cursor()
        cursor.execute(sql, sqlParms)
        row_headers=[x[0] for x in cursor.description]
        rv = cursor.fetchall()
        x1 = {keyLabel: []}
        for result in rv:
            x1[keyLabel].append(dict(zip(row_headers,result)))
        cnx.commit()
        cursor.close()
        return x1
    except:
        print("Database initial connect failed: {}".format(err))
        return {"Successful": False}

@app.route('/flights/<origin>/<dest>/<fl_date>')
def flightData(origin, dest, fl_date):
    query = """
        SELECT fl_date, dep_time, carrier, fl_num, origin, dest
        FROM travel.flights
        WHERE origin = ?
        AND dest = ?
        AND fl_date = ?
        ORDER BY fl_date, dep_time
    """
    parms = (origin, dest, fl_date)
    x1 = runSelectSQL(query, parms, "FlightData")
    return x1

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8081)